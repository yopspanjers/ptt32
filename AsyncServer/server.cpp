#include "server.h"

Server::Server(QObject *parent) : QTcpServer(parent)
{
}

void Server::StartServer()
{
    if(listen(QHostAddress::LocalHost,1234))
        {
            qDebug() << "Server Started";
        }
        else
        {
            qDebug() << "Server could not be started";
        }
    qDebug() << serverAddress() << serverPort();
}

void Server::incomingConnection(qintptr handle)
{
    Device *device = new Device(this);
    device->SetSocket(handle);
}
