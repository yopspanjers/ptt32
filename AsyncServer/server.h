#ifndef SERVER_H
#define SERVER_H

#include "device.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    void StartServer();
    void incomingConnection(qintptr handle) Q_DECL_OVERRIDE;

signals:

public slots:
};

#endif // SERVER_H
