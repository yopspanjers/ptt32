#include "device.h"

Device::Device(QObject *parent) : QObject(parent)
{
    QThreadPool::globalInstance()->setMaxThreadCount(15);

}

void Device::SetSocket(int Descriptor)
{
    socket = new QTcpSocket(this);

    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));

    socket->setSocketDescriptor(Descriptor);

    qDebug() << Descriptor << "Client connected";
    //socket->write("Hello World!");
}

void Device::connected()
{
    qDebug() << "Client conntected event";
}

void Device::disconnected()
{
    qDebug() << "Client Disconnected";
}

void Device::readyRead()
{
    qDebug() <<  socket->readAll();
    socket->write("Hello back");
}

void Device::TaskResult(int Number)
{

}
