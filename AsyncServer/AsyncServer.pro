QT += core network
QT -= gui

CONFIG += c++11

TARGET = AsyncServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    server.cpp \
    device.cpp

HEADERS += \
    server.h \
    device.h

FORMS +=
