#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    bool SendMessage(QString msg);

public slots:
    void connected();
    void disconnected();
    void readyRead();
    void SendMessageUI();

};

#endif // MAINWINDOW_H
