#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    socket = new QTcpSocket(this);
    socket->connectToHost("127.0.0.1", 1234);

    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(ui->btnSend, SIGNAL(clicked(bool)),this,SLOT(SendMessageUI()));
    SendMessage("Hello Server!");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connected()
{

}

void MainWindow::disconnected()
{

}

void MainWindow::readyRead()
{
    ui->textIn->append(socket->readAll());
}


bool MainWindow::SendMessage(QString msg)
{
    ui->textOut->append(msg);
    socket->write(msg.toLatin1().data());
    return true;
}

void MainWindow::SendMessageUI()
{
    MainWindow::SendMessage(ui->inputBoxMessage->text());
}
